var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var helmet = require('helmet');
var logger = require('morgan');
require("dotenv").config();

var indexRouter = require('./routes/index');

var app = express();

app.use(function(req,res,next) {
  res.setHeader('Cache-Control', 'no-store');
  res.setHeader('Pragma', 'no-cache');
  next();
})
app.use(helmet());
app.use(cors({
  origin: "*",
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
}));
if (process.env.NODE_ENV != 'production' && process.env.HIDE_LOGGER && parseInt(process.env.HIDE_LOGGER) == 0) app.use(logger('combined'));

app.use('/', express.static('public', {
  setHeaders: function(res, path) {
    res.setHeader("Content-Security-Policy", "");
  }
}));

app.use(indexRouter);

// Catch "no Route Found" => 404 - Not Found!
app.use(function (req, res, next) {
  if (!(res.headersSent)) res.status(404).json({
    message: "Route not found."
  });
});

// error handler
app.use(async function (err, req, res, next) {
  // Error was not handled
  if (!res.headersSent) res.status(500).json({ message: "Internal Server Error." });
  throw err;
});

module.exports = app;