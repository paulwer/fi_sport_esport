var express = require('express');
var router = express.Router();

/* GET Discord Link */
router.get('/discord', function(req, res, next) {
  res.redirect(process.env.DISCORD_URL)
});

/* GET Twitch Link */
router.get('/live', function(req, res, next) {
  res.redirect(process.env.TWITCH_URL);
});
router.get('/stream', function(req, res, next) {
  res.redirect(process.env.TWITCH_URL);
});
router.get('/twitch', function(req, res, next) {
  res.redirect(process.env.TWITCH_URL);
});

/* GET YouTube Link */
router.get('/youtube', function(req, res, next) {
  res.redirect(process.env.YOUTUBE_URL);
});

/* GET Turnier Umfrage Link */
router.get('/feedback', function(req, res, next) {
  res.redirect("https://forms.gle/zhXvXHw7vswq9aBE8");
});

module.exports = router;
